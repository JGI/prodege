FROM bioperl/bioperl
FROM rocker/r-ver:3.1.0

LABEL Maintainer Kecia Duffy, kmduffy@lbl.gov

#
# If you need to add more software, try to follow this style:
# - use --no-install-recommends to reduce bloat
# - use command-chaining to keep it all in one cache layer
# - use purge to remove stuff afterwards if you can
#
# Set the DEBIAN_FRONTEND environment variable as shown to avoid problems
# being prompted for input
#
# Blast+ 2.2.28
# Perl 5.16.0 (with modules Bio::SeqIO and Bio::Perl)
# Prodigal 2.50
# R 3.0.1
# The BLASTN_EXE envirnonmental variable is used to run blastn, R_EXE to run R,
# and PRODIGAL_EXE to run prodigal. 


RUN apt-get update -y && \
    apt-get install -y wget ca-certificates --no-install-recommends && \
	apt-get install make && \
	wget https://github.com/hyattpd/Prodigal/releases/download/v2.6.3/prodigal.linux && \
    chmod 755 prodigal.linux && \
    mv prodigal.linux /usr/bin/prodigal

	
#NCBI Blast download
RUN mkdir /opt/blast \ 
    && curl ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.28/ncbi-blast-2.2.28+-x64-linux.tar.gz \ 
	| tar -zxC /opt/blast --strip-components=1 
ENV PATH /opt/blast/bin:$PATH

RUN apt-get install -y --no-install-recommends openjdk-7-jdk


## Now install Prodege
COPY prodege-2.2.tgz /tmp
RUN tar -zxf /tmp/prodege-2.2.tgz 
#cd prodege-2.2 
#install extra R packages
RUN echo "r <- getOption('repos'); r['CRAN'] <- 'http://cran.us.r-project.org'; options(repos = r);" > ~/.Rprofile
RUN wget -O prodege-2.2/lib/bigmemory.sri.tar.gz http://cran.r-project.org/src/contrib/Archive/bigmemory.sri/bigmemory.sri_0.1.2.tar.gz && \
wget -O prodege-2.2/lib/bigmemory.tar.gz http://cran.r-project.org/src/contrib/Archive/bigmemory/bigmemory_4.4.5.tar.gz && \
wget -O prodege-2.2/lib/BH.tar.gz http://cran.r-project.org/src/contrib/Archive/BH/BH_1.54.0-1.tar.gz && \
wget -O prodege-2.2/lib/biganalytics.tar.gz http://cran.r-project.org/src/contrib/Archive/biganalytics/biganalytics_1.1.1.tar.gz 

RUN R CMD INSTALL -l prodege-2.2/lib prodege-2.2/lib/BH.tar.gz && \
R CMD INSTALL -l prodege-2.2/lib prodege-2.2/lib/bigmemory.sri.tar.gz && \
R CMD INSTALL -l prodege-2.2/lib prodege-2.2/lib/bigmemory.tar.gz && \
R CMD INSTALL -l prodege-2.2/lib prodege-2.2/lib/biganalytics.tar.gz 

RUN rm prodege-2.2/lib/bigmemory.sri.tar.gz && \
rm prodege-2.2/lib/BH.tar.gz && \
rm prodege-2.2/lib/bigmemory.tar.gz 



# Download files from NCBI
RUN mkdir prodege-2.2/NCBI-nt-tax && \
wget -O prodege-2.2/NCBI-nt-tax/taxdump.tar.gz ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz  && \
cd prodege-2.2/NCBI-nt-tax/ && \
tar -zxvf taxdump.tar.gz && \
rm /prodege-2.2/NCBI-nt-tax/taxdump.tar.gz && \
# Create the file for Prodege
perl /prodege-2.2/bin/01.createTaxSpeciesfile.pl  /prodege-2.2/NCBI-nt-tax && \

#get the img tax file from the portal
mkdir /prodege-2.2/IMG-tax && \
cd /prodege-2.2/IMG-tax && \
wget -O img_taxonomy.txt https://portal.nersc.gov/dna/microbial/omics-prodege/IMG-tax/img_taxonomy.txt && \





# Delete unused files
rm /prodege-2.2/NCBI-nt-tax/*.dmp && \
rm /prodege-2.2/NCBI-nt-tax/*.prt && \
rm /prodege-2.2/NCBI-nt-tax/readme.txt

ENV BLASTN_EXE=/opt/blast/bin
ENV PRODIGAL_EXE=/usr/bin/prodigal
ENV R_EXE=/usr/local/bin/R



#RUN apt-get purge -y --auto-remove